/*
 * utilities.c
 *
 *  Created on: Apr 5, 2017
 *      Author: Siddhant
 */

/* Returns next target coords
 * found by linear interpolation
 */

#include "utilities.h"
#include "math.h"

struct coordinates interpolate(uint16_t current_x, uint16_t current_y, float slope, bool vertical_motion_flag, bool moving_up, bool moving_right)
{
    int x, y;

    if (vertical_motion_flag)
    {
        x = current_x;
        y = moving_up ? (current_y + Y_STEP_SIZE) : (current_y - Y_STEP_SIZE);
    }
    else
    {
        x = moving_right ? (current_x + X_STEP_SIZE) : (current_x - X_STEP_SIZE);
        y = round(current_y + (x - current_x)*slope);
    }

    struct coordinates next_coords =
    {
        .x = x,
        .y = y,
    };

    return next_coords;
}
