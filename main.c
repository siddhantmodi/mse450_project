#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "math.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "driverlib/qei.h"
#include "utils/uartstdio.h"
#include "button.h"
#include "lookup_table.h"
#include "utilities.h"
#include "peripherals.h"

#define MAX_PWM_ADJUST							150

#define LOOKUP_INDEX_MOTOR_1					1
#define LOOKUP_INDEX_MOTOR_2					0
#define ENCODER_OFFSET							300

#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

volatile uint32_t m_ui32_load;
static uint32_t m_motor_1_target = 0; // Encoder ticks for theta 5
static uint32_t m_motor_2_target = 0; // Encoder ticks for theta 2

bool is_making_art = false;
static int current_x = 0, current_y = 0;
volatile bool m_is_in_motion = false;

int Kp = 25;
int Ki = 0;
int Kd = 5;

static void PID_control()
{
	bool is_motor_1_in_motion = true, is_motor_2_in_motion = true;
	static int last_error_1 = 0;
	static int last_error_2 = 0;
	int position_error_1 = m_motor_1_target - QEIPositionGet(QEI0_BASE);
	int position_error_2 = m_motor_2_target - QEIPositionGet(QEI1_BASE);
	int derivative_error_1 = (position_error_1 - last_error_1) / TIMER_PERIOD_PID_MS;
	int derivative_error_2 = (position_error_2 - last_error_2) / TIMER_PERIOD_PID_MS;
	static int integral_error_1 = 0;
	static int integral_error_2 = 0;

	UARTprintf("Encoder: (%d, %d)\n", QEIPositionGet(QEI0_BASE), QEIPositionGet(QEI1_BASE));

	int pwm_adjust = 0;

	if (fabs(position_error_1) <= 3) // If position within threshold, stop motor
	{
		PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, 1 * m_ui32_load / 1000);
		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, 0x00);
		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0x00);
		is_motor_1_in_motion = false;
	}
	else
	{
		m_is_in_motion = true;
		pwm_adjust = Kp*position_error_1 + Ki*integral_error_1 + Kd*derivative_error_1;

		if (fabs(pwm_adjust) > MAX_PWM_ADJUST) // If duty cycle is large, set to current max limit
		{
			pwm_adjust = pwm_adjust > 0 ? MAX_PWM_ADJUST : -MAX_PWM_ADJUST;
		}
		else
		{
			integral_error_1 += ((float)TIMER_PERIOD_PID_MS*position_error_1);
		}

		if (pwm_adjust > 0) // Spin motor in counter-clockwise direction
		{
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, 0x00);
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0x10);
			PWMPulseWidthSet(PWM1_BASE, PWM_PIN_MOTOR_1, pwm_adjust*m_ui32_load/1000);
		}
		else // Spin motor in clockwise direction
		{
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0x00);
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, 0x20);
			PWMPulseWidthSet(PWM1_BASE, PWM_PIN_MOTOR_1, fabs(pwm_adjust)*m_ui32_load/1000);
		}
	}

	if (fabs(position_error_2) <= 3) // If position within threshold, stop motor
	{
		PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, 1 * m_ui32_load / 1000);
		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, 0x00);
		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, 0x00);
		is_motor_2_in_motion = false;
	}
	else
	{
		m_is_in_motion = true;
		pwm_adjust = Kp*position_error_2 + Ki*integral_error_2 + Kd*derivative_error_2;

		if (fabs(pwm_adjust) > MAX_PWM_ADJUST) // If duty cycle is large, set to current max limit
		{
			pwm_adjust = pwm_adjust > 0 ? MAX_PWM_ADJUST : -MAX_PWM_ADJUST;
		}
		else
		{
			integral_error_2 += ((float)TIMER_PERIOD_PID_MS*position_error_2);
		}

		if (pwm_adjust > 0) // Spin motor in counter-clockwise direction
		{
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, 0x00);
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, 0x08);
			PWMPulseWidthSet(PWM1_BASE, PWM_PIN_MOTOR_2, pwm_adjust*m_ui32_load/1000);
		}
		else // Spin motor in clockwise direction
		{
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, 0x00);
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, 0x04);
			PWMPulseWidthSet(PWM1_BASE, PWM_PIN_MOTOR_2, fabs(pwm_adjust)*m_ui32_load/1000);
		}
	}

	last_error_1 = position_error_1; // Update the errors
	last_error_2 = position_error_2;

	if (!is_motor_1_in_motion && !is_motor_2_in_motion) // If both motors are stopped, update global flag
	{
		m_is_in_motion = false;
	}
}

static void _timer_handler(void)
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

	PID_control();
}

/*
 * Called when a new line is to be drawn
 * Exits when the marker is at target position
 */
void draw(int target_x, int target_y)
{
    while (current_x != target_x || current_y != target_y)
    {
    	/* Calculate and validate slope. Check direction of motion. */
		bool vertical_motion_flag = false;
		bool moving_up = false;
		bool moving_right = false;

		float slope = 0;

		if (target_x == current_x)
		{
			vertical_motion_flag = true;
			moving_up = (target_y > current_y) ? true : false;
		}
		else
		{
			slope = ((float)(target_y - current_y)/(target_x - current_x));
			moving_right = (target_x > current_x) ? true : false;
		}

        /* Get next intermediate position from interpolate. */
        struct coordinates next_coords = interpolate(current_x, current_y, slope, vertical_motion_flag, moving_up, moving_right);

        /* Lookup to find target encoder positions - CRITICAL REGION. */
        TimerIntDisable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
        m_motor_1_target = encoder_lookup[next_coords.x][next_coords.y][LOOKUP_INDEX_MOTOR_1] + ENCODER_OFFSET;
        m_motor_2_target = encoder_lookup[next_coords.x][next_coords.y][LOOKUP_INDEX_MOTOR_2] + ENCODER_OFFSET;
        TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

        /* Wait while moving. */
        while(m_is_in_motion);

        /* Update current x and y. */
        current_x = next_coords.x;
        current_y = next_coords.y;
    }

    return;
}

/*
 * Lower the table
 * In an arbitrary path move to the target location (no interpolation)
 * Raise table when at target location
 * Exit
 */
void move(int x_target, int y_target)
{
    /* Get and set encoder values for target position - CRITICAL REGION. */
	TimerIntDisable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	m_motor_1_target = encoder_lookup[x_target][y_target][LOOKUP_INDEX_MOTOR_1] + ENCODER_OFFSET; // Theta5
	m_motor_2_target = encoder_lookup[x_target][y_target][LOOKUP_INDEX_MOTOR_2] + ENCODER_OFFSET; // Theta1
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    
    /* Wait until position reached. */
    while(m_is_in_motion);
    
    /* Update current position globals. */
    current_x = x_target;
    current_y = y_target;
    
    return;
}

static void my_button_callback(enum button_event _button_event)
{
	switch (_button_event)
	{
		case BUTTON_EVENT_SHORT_PRESS:
			is_making_art = true;
			break;
		case BUTTON_EVENT_LONG_HOLD:
			move(0,0);
			break;
		case BUTTON_EVENT_DOUBLE_PRESS:
			move(50, 40);
			break;
		default:
			break;
	}
}

void make_art()
{
	draw(150, 5);
	draw(50, 5);
	draw(150, 40);
	draw(50, 40);
}

static void _timer_register_and_enable()
{
	TimerIntRegister(TIMER0_BASE, TIMER_PID, _timer_handler);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	TimerEnable(TIMER0_BASE, TIMER_PID);
}

/*
 * main.c
 */
void main(void)
{
	/* Configure the clock to run at 40 MHz. */
	SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
	GPIO_init();
	m_ui32_load = PWM_init();
	UART_init();
	QEI_init();
	QEI_position_init(encoder_lookup[0][0][LOOKUP_INDEX_MOTOR_1] + ENCODER_OFFSET,
			encoder_lookup[0][0][LOOKUP_INDEX_MOTOR_2] + ENCODER_OFFSET);
	TIMER_init();
	_timer_register_and_enable();
	BUTTON_init();
	BUTTON_enable(my_button_callback);

	IntMasterEnable(); // Enables interrupts in the NVIC

	while (1)
	{
		if (is_making_art)
		{
			make_art();
			is_making_art = false;
		}
	}
}
