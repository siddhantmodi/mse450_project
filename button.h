enum button_event {
	BUTTON_EVENT_SHORT_PRESS,
	BUTTON_EVENT_LONG_HOLD,
	BUTTON_EVENT_DOUBLE_PRESS,
	BUTTON_EVENT_DOUBLE_PRESS_LONG_HOLD,
};

/* Type definition for the button callback to be passed onto the BUTTON_enable function. */
typedef void (* button_callback_t)(enum button_event e);

void BUTTON_init(void);

void BUTTON_enable(button_callback_t callback);

void BUTTON_disable(void);
