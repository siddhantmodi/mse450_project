/*
 * peripherals.c
 *
 *  Created on: Apr 9, 2017
 *      Author: Siddhant
 */

#include "peripherals.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/systick.h"
#include "driverlib/pin_map.h"
#include "driverlib/button.h"
#include "driverlib/uart.h"
#include "driverlib/qei.h"
#include "utils/uartstdio.h"

void GPIO_init()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	IntEnable(INT_GPIOF);

	/* Initialize the LEDs for testing. */
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5);

	return;
}

void UART_init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTStdioConfig(0, 115200, SysCtlClockGet());
    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    IntEnable(INT_UART0); //enable the UART interrupt
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT); //only enable RX and TX interrupts

    return;
}

void QEI_init(void)
{
	/* First QEI Peripheral. */

	/* Enable QEI Peripherals. */
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);

	/* Unlock GPIOD7 - Like PF0 its used for NMI - Without this step it doesn't work. */
	HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTD_BASE + GPIO_O_CR) |= 0x80;
	HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = 0;

	/* Set Pins to be PHA1 and PHB1. */
	GPIOPinConfigure(GPIO_PD6_PHA0);
	GPIOPinConfigure(GPIO_PD7_PHB0);

	/* Set GPIO pins for QEI. PhA0 -> PD6, PhB0 -> PD7. */
	GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 | GPIO_PIN_7);

	/* Disable peripheral and int before configuration/ */
	QEIDisable(QEI_MOTOR_1);
	QEIIntDisable(QEI_MOTOR_1, QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER | QEI_INTINDEX);

	/* Configure quadrature encoder, use the top limit of 1200 for this encoder. */
	QEIConfigure(QEI_MOTOR_1, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_NO_RESET | QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 1200);

	/* Enable the quadrature encoder. */
	QEIEnable(QEI_MOTOR_1);

	/* Set current position to 0 on init. */
	QEIPositionSet(QEI_MOTOR_1,  0);

	/* Second QEI Peripheral. */

	/* Enable QEI Peripherals. */
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);

	/* Set Pins to be PHA1 and PHB1. */
	GPIOPinConfigure(GPIO_PC5_PHA1);
	GPIOPinConfigure(GPIO_PC6_PHB1);

	/* Set GPIO pins for QEI. PhA1 -> PC5, PhB1 -> PC6. */
	GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_5 | GPIO_PIN_6);

	/* Disable peripheral and int before configuration. */
	QEIDisable(QEI_MOTOR_2);
	QEIIntDisable(QEI_MOTOR_2, QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER | QEI_INTINDEX);

	/* Configure quadrature encoder, use a top limit of 1200 for this encoder. */
	QEIConfigure(QEI_MOTOR_2, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_NO_RESET | QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 1200);

	/* Enable the quadrature encoder. */
	QEIEnable(QEI_MOTOR_2);

	/* Set current position to 0 on init. */
	QEIPositionSet(QEI_MOTOR_2, 0);

	return;
}

void QEI_position_init(unsigned int motor_1_encoder, unsigned int motor_2_encoder)
{
	QEIPositionSet(QEI_MOTOR_1, motor_1_encoder);
	QEIPositionSet(QEI_MOTOR_2, motor_2_encoder);

	return;
}

void TIMER_init(void)
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
	TimerLoadSet(TIMER0_BASE, TIMER_PID, ((float)TIMER_PERIOD_PID_MS/1000)*SysCtlClockGet());
	IntEnable(INT_TIMER0A);
	IntPrioritySet(INT_TIMER0A, 0);

	return;
}

uint32_t PWM_init(void)
{
    /* Uses PWM module 1, pins D0 and D1 */

	/*  Set PWM clock to run at 625 KHz
	 *  Enable PWM peripheral.
	 *  Enable GPIO D peripheral.
	 *  Calculate and load a value into the PWM timer
	 *  such that it takes 0.01 sec at 625 KHz to count down
	 *  Set pulse width to 1.5ms
	 *  Enable PWM
	 */
    volatile uint32_t ui32_pwm_clock;

    SysCtlPWMClockSet(SYSCTL_PWMDIV_64);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    GPIOPinConfigure(GPIO_PD0_M1PWM0);
	GPIOPinConfigure(GPIO_PD1_M1PWM1);

	ui32_pwm_clock = SysCtlClockGet() / 64;
    uint32_t ui32_load = (ui32_pwm_clock / PWM_FREQUENCY) - 1; // loaded value runs out 100 times per second
    PWMGenConfigure(PWM1_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN);
	PWMGenPeriodSet(PWM1_BASE, PWM_GEN_0, ui32_load);

    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, 1 * ui32_load / 1000);
	PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, 1 * ui32_load / 1000);

    PWMOutputState(PWM1_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT, true);
	PWMGenEnable(PWM1_BASE, PWM_GEN_0);

	return ui32_load;
}
