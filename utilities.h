/*
 * utilities.h
 *
 *  Created on: Apr 5, 2017
 *      Author: Siddhant
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <stdint.h>
#include <stdbool.h>

#define X_STEP_SIZE 							1
#define Y_STEP_SIZE 							1

struct coordinates {
   int x;
   int y;
};

/* Returns next target coords
 * found by linear interpolation
 */
struct coordinates interpolate(uint16_t current_x, uint16_t current_y, float slope, bool vertical_motion_flag, bool moving_up, bool moving_right);

#endif /* UTILITIES_H_ */
