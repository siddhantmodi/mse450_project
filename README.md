# Project Description
This is a project that was done for MSE 450 - Real Time Control Systems at Simon Fraser University. It is a 2D Parallel SCARA manipulator based XY Plotter that is controlled by a Tiva C Series TM4C123GXL development board. 
The frame is constructed out of sheet steel and the arms are 3D printed. The motion is controlled by 2x Pololu Micro Metal Gearmotors equipped with magnetic encoders. An L298 motor driver breakout board is used to control the motors.
The inverse kinematics were processed using MATLAB and a Python script was used to reshape the data to match a C-style array. All the firmware has been written in C. 

Team Members:
- [Siddhant Modi](https://www.linkedin.com/in/siddhantmodi/)
- [Vadim Tsarkov] (https://www.linkedin.com/in/vtsarkov/)
- [Cameron Jinks] (https://www.linkedin.com/in/cameron-jinks-61773b29/)

### YouTube link: https://youtu.be/IgjLm_XjOLk

# L298 Motor Driver wiring
- Connect 5V out from driver to pins labelled 5V
- Connect GND pins to GND of power supply. Make sure that the GND of the MCU is connected to the same pin (Common Ground)
- Connect EnA to PD0 and EnB to PD1 - they are the PWM pins
- In1 and In2 go to PE4 and PE5 - these pins are for direction control
- In3 and In4 to PE3 and PE2 - these pins are for direction control
- Motor1 A and B go to PD7 and PD6
- Motor2 A and B go to PC6 and PC5

# Button library
- To use the button library, add the button.c and button.h files to your CCS project.

### Creating symlinks to files on Windows
My CCS Workspace is in a folder different than my main git repo. I don't want to change my CCS Workspace since I already have everything set up there. So add my source code to git, I simply created a symlink to the code files and put them in my git repo. That way, the files actually live in the Workspace but links to them exist in the repo. 

The procedure for doing that is:

1. Run cmd as admin
. 

mklink C:\ti\MyWorkspace\my_project\main.c
 E:\my_git_repo\my_project\main.c 

This will create a "shortcut" to the main.c file in the git repo and will update automatically any time a change is made to the main.c file in the workspace.