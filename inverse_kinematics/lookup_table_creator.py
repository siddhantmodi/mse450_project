import csv

TARGET_FILENAME='lookup_table.txt'


def erase():
    with open(TARGET_FILENAME, 'w') as file:
        file.write('')

def append(string):
    with open(TARGET_FILENAME, 'a') as file:
        file.write(string)

def get_matlab_files():
    with open('theta2.csv', 'rb') as f:
        reader = csv.reader(f)
        content = list(reader)

    theta2 = []
    for angle in content:
        theta2.append(int(angle[0]))


    with open('theta5.csv', 'rb') as f:
        reader = csv.reader(f)
        content = list(reader)

    theta5 = []
    for angle in content:
        theta5.append(int(angle[0]))

    return theta2, theta5

if __name__ == "__main__":

    theta2, theta5 = get_matlab_files()

    x_max = 171
    y_max = 55

    erase()

    append('{\n')

    for x in range(x_max):

        append('    {')

        for y in range(y_max):

            append('{')
            append('{0},{1}'.format(theta2[y_max*x+y], theta5[y_max*x+y]))

            if(y is y_max-1):

                append('}')
            else:
                append('}, ')

        append('},\n')


    append('};\n')






    print 'x'
