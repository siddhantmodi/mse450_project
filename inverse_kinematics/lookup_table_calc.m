clc
clear all
close all

%% Geometric constants
r1 = 1016; % 1/10*mm
r2 = 1153;
r3 = 1153;
r4 = 1153;
r5 = 1153;


deg_per_tick = 360/1200;
origin = [-365, 1300];
ll_x = 0;
ll_y = 0;
ur_x = 1700;
ur_y = 540;


%% Inverse Kinemtics
theta2 = [];
theta5 = [];

output = [];

for x_i=ll_x:10:ur_x
    for y_i=ll_y:10:ur_y
        x=x_i/10;
        y=y_i/10;
        theta2 = [theta2; round(radtodeg(theta_2(x_i+origin(1), y_i+origin(2), r2, r3))/deg_per_tick)];
        theta5 = [theta5; round(radtodeg(theta_5(x_i+origin(1), y_i+origin(2), r1, r4, r5))/deg_per_tick)];
        
        output(x+1, y+1, 1) = radtodeg(theta_2(x_i+origin(1), y_i+origin(2), r2, r3));
        output(x+1, y+1, 2) = radtodeg(theta_5(x_i+origin(1), y_i+origin(2), r1, r4, r5));
    end
end

csvwrite('theta2.csv',theta2)
csvwrite('theta5.csv',theta5)



