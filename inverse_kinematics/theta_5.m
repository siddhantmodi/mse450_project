function [output_args] = theta_5(x, y, r1, r4, r5)
    output_args = -acos(((x - r1)^2 + y^2 + r5^2 - r4^2)/(2*r5*sqrt((x - r1)^2 + y^2))) + atan2(y, (x-r1));
end