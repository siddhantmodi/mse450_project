function [output_args] = theta_2(x, y, r2, r3)
    output_args = acos((x^2 + y^2 + r2^2 - r3^2)/(2*r2*sqrt(x^2 + y^2))) + atan2(y, x);
end

