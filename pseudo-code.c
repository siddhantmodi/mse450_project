
#include <stdio.h>
#include <stdlib.h>

#define X_STEP_SIZE 1
#define Y_STEP_SIZE 1

static int motor1_target, motor2_target;
static int current_x = 0, current_y = 0;

struct coordinates {
   int x;
   int y;
};

int b[2][55][2] = {
    {{533,340}, {531,340}, {530,341}, {529,341}, {527,341}, {526,342}, {525,342}, {523,342}, {522,343}, {521,343}, {519,344}, {518,344}, {516,345}, {515,345}, {514,346}, {512,346}, {511,347}, {510,347}, {508,348}, {507,349}, {506,349}, {504,350}, {503,351}, {501,351}, {500,352}, {499,353}, {497,354}, {496,355}, {494,356}, {493,356}, {492,357}, {490,358}, {489,359}, {487,361}, {486,362}, {485,363}, {483,364}, {482,365}, {480,367}, {479,368}, {477,370}, {476,371}, {474,373}, {473,374}, {471,376}, {470,378}, {468,380}, {467,383}, {465,385}, {464,388}, {462,391}, {461,394}, {459,398}, {458,403}, {456,410}},
    {{532,338}, {530,339}, {529,339}, {528,339}, {526,340}, {525,340}, {524,340}, {522,341}, {521,341}, {520,342}, {518,342}, {517,342}, {515,343}, {514,343}, {513,344}, {511,344}, {510,345}, {509,346}, {507,346}, {506,347}, {505,347}, {503,348}, {502,349}, {500,349}, {499,350}, {498,351}, {496,352}, {495,353}, {494,354}, {492,354}, {491,355}, {489,356}, {488,357}, {486,358}, {485,360}, {484,361}, {482,362}, {481,363}, {479,364}, {478,366}, {476,367}, {475,369}, {474,370}, {472,372}, {471,374}, {469,376}, {468,378}, {466,380}, {465,382}, {463,384}, {461,387}, {460,390}, {458,394}, {457,398}, {455,403}},
};


/* Returns next target coords
 * found by linear interpolation
 */
struct coordinates interpolate(float slope, bool vertical_motion_flag) 
{
    int x, y;

    if (vertical_motion_flag) 
    {
        x = current_x;
        y = current_y + Y_STEP_SIZE;
    }
    else 
    {
        x = current_x + X_STEP_SIZE;
        y = round(current_y + (.x - current_x)*slope);
    }

    struct coordinates next_coords = 
    {
        .x = x;
        .y = y;
    }

    return next_coords;
}

/* Called when a new line is to be drawn
 * Exits when the marker is at target position
 */
void draw(int target_x, int target_y)
{
    
    // calculate and validate slope
    bool vertical_motion_flag = 0;
    float slope = 0;

    if (target_x == current_x) 
    {
        vertical_motion_flag = 1;
    }
    else 
    {
        slope = ((target_y - current_y)/(target_x - current_x));
    }

    while (current_x != target_x && current_y != target_y)
    {
        // get next intermediate position from interpolate(x_target, y_target)
        struct coordinates next_coords = interpolate(slope, vertical_motion_flag);
        
        // lookup to find target encoder positions
        motor1_target = lookup[next_coords.x][next_coords.y][0];
        motor2_target = lookup[next_coords.x][next_coords.y][1]
        
        // move to target encoder values
        
        // wait while moving

        // update current x and y
        current_x = next_coords.x;
        current_y = next_coords.y;
    }
        
    return void;
}

/* Lower the table
 * In an arbitrary path move to the target location
 * Raise table when at target location
 * Exit
 */
void move(int x_target, int y_target)
{
    // lower table
    
    // get encoder values for target position
    
    // set global motor targets
    
    // wait until position reached
    
    // update current position globals
    
    // lower table
    
    return void;
}


int main() {
    
    int x_init = 20;
    int y_init = 20;
    
    int x_target = 40;
    int y_target = 50;
    
    
    
    
    
    return 0;
}