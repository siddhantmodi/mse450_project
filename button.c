#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "button.h"

#define BUTTON_DEBOUNCE_TIMEOUT_MS				50      // Timer3
#define BUTTON_LONG_HOLD_TIMEOUT_MS 			1000    // Timer4
#define BUTTON_DOUBLE_PRESS_TIMEOUT_MS			500     // Timer5

#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

enum button_state {
	BUTTON_STATE_STABLE,
	BUTTON_STATE_HOLDING,
	BUTTON_STATE_WAITING_FOR_DOUBLE_PRESS,
};

enum button_position {
	BUTTON_PRESSED,
	BUTTON_RELEASED,
};

static enum button_position m_current_button_position = BUTTON_PRESSED;
static enum button_state m_button_state = BUTTON_STATE_STABLE;
static button_callback_t m_callback;

static void _button_interrupt_handler(void)
{
	GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);

	/* Disable the button pin interrupt. */
	GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);

	/* Start the debounce timer. */
	TimerEnable(TIMER3_BASE, TIMER_A);
}

static void _debounce_timer_handler(void)
{
	/* Check if the button position has indeed changed. */
	enum button_position previous_button_position = m_current_button_position;
	m_current_button_position = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4) ? BUTTON_PRESSED : BUTTON_RELEASED;

	if (m_current_button_position != previous_button_position)
	{
		if (m_current_button_position == BUTTON_PRESSED)
		{
			switch (m_button_state)
			{
				case BUTTON_STATE_STABLE:
					/* This case fires the timer for a double press. */
					m_button_state = BUTTON_STATE_WAITING_FOR_DOUBLE_PRESS;
					TimerDisable(TIMER4_BASE, TIMER_A);
					TimerEnable(TIMER5_BASE, TIMER_A);
					break;
				case BUTTON_STATE_WAITING_FOR_DOUBLE_PRESS:
					/* This case handles a double short press. */
					m_button_state = BUTTON_STATE_STABLE;
					TimerDisable(TIMER4_BASE, TIMER_A);
					m_callback(BUTTON_EVENT_DOUBLE_PRESS);
					break;
				default:
					/* This case is for when the button returns to an UP position after a long hold. */
					m_button_state = BUTTON_STATE_STABLE;
					break;
			}
		}
		else
		{
			if (m_button_state == BUTTON_STATE_STABLE)
			{
				/* This case handles a long hold. */
				TimerEnable(TIMER4_BASE, TIMER_A);
			}
			else
			{
				/* This case handles a double press long hold. */
				TimerDisable(TIMER5_BASE, TIMER_A);
				TimerEnable(TIMER4_BASE, TIMER_A);
			}
		}
	}

	/* Re-enable the button interrupt. */
	GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);

	/* Clear the interrupt. */
	TimerIntClear(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
}

static void _timer_long_hold_handler(void)
{
	if (m_button_state == BUTTON_STATE_STABLE)
	{
		m_callback(BUTTON_EVENT_LONG_HOLD);
	}
	else
	{
		m_callback(BUTTON_EVENT_DOUBLE_PRESS_LONG_HOLD);
	}

	m_button_state = BUTTON_STATE_HOLDING;
	TimerIntClear(TIMER4_BASE, TIMER_TIMA_TIMEOUT);
}

static void _timer_short_press_handler(void)
{
	/* If this ISR is triggered, it means there was a single short press. */
	m_button_state = BUTTON_STATE_STABLE;
	m_callback(BUTTON_EVENT_SHORT_PRESS);

	TimerIntClear(TIMER5_BASE, TIMER_TIMA_TIMEOUT);
}

void BUTTON_init()
{
	/* Timer3, Timer4 and Timer5 are used by this driver. */

	/*  Enable the Timer peripheral.
	 *  Configure the timers to One-Shot mode.
	 *  Set the timer timeouts.
	 *  Enable the interrupts.
	 *  Configure the interrupts for the timer timeouts.
	 */

	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
	TimerConfigure(TIMER3_BASE, TIMER_CFG_ONE_SHOT);
	TimerLoadSet(TIMER3_BASE, TIMER_A, (((float)BUTTON_DEBOUNCE_TIMEOUT_MS)/1000)*SysCtlClockGet());
	IntEnable(INT_TIMER3A);
	TimerIntRegister(TIMER3_BASE, TIMER_A, _debounce_timer_handler);
	TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER4);
	TimerConfigure(TIMER4_BASE, TIMER_CFG_ONE_SHOT);
	TimerLoadSet(TIMER4_BASE, TIMER_A, (((float)BUTTON_LONG_HOLD_TIMEOUT_MS)/1000)*SysCtlClockGet());
	IntEnable(INT_TIMER4A);
	TimerIntRegister(TIMER4_BASE, TIMER_A, _timer_long_hold_handler);
	TimerIntEnable(TIMER4_BASE, TIMER_TIMA_TIMEOUT);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5);
	TimerConfigure(TIMER5_BASE, TIMER_CFG_ONE_SHOT);
	TimerLoadSet(TIMER5_BASE, TIMER_A, (((float)BUTTON_DOUBLE_PRESS_TIMEOUT_MS)/1000)*SysCtlClockGet());
	IntEnable(INT_TIMER5A);
	TimerIntRegister(TIMER5_BASE, TIMER_A, _timer_short_press_handler);
	TimerIntEnable(TIMER5_BASE, TIMER_TIMA_TIMEOUT);

	/* Enable the GPIO pin for the button. */
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	IntEnable(INT_GPIOF);

	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
	GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4,
	        GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

	GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);
	GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);
	GPIOIntRegister(GPIO_PORTF_BASE, _button_interrupt_handler);
	GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_BOTH_EDGES);

	/* Read the current status of the pin and store it. */
	m_current_button_position = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4) ? BUTTON_PRESSED : BUTTON_RELEASED;

	/* Initialize the LEDs for testing. */
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
}

void BUTTON_enable(button_callback_t button_callback)
{
	m_callback = button_callback;

	/* Enable the button interrupt. */
	GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);
}
