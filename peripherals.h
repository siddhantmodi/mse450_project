/*
 * peripherals.h
 *
 *  Created on: Apr 9, 2017
 *      Author: Siddhant
 */

#ifndef PERIPHERALS_H_
#define PERIPHERALS_H_

#include <stdint.h>
#include "stdbool.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"

#define PWM_FREQUENCY                           400
#define PWM_PIN_MOTOR_1							PWM_OUT_0
#define PWM_PIN_MOTOR_2							PWM_OUT_1
#define PWM_PIN_ELECTROMAGNET					PWM_OUT_2

#define QEI_MOTOR_1								QEI0_BASE
#define QEI_MOTOR_2								QEI1_BASE

#define TIMER_PID								TIMER_A
#define TIMER_PERIOD_PID_MS						5

void GPIO_init();
void UART_init();
void QEI_init();
void QEI_position_init(unsigned int motor_1_encoder, unsigned int motor_2_encoder);
void TIMER_init();
uint32_t PWM_init();

#endif /* PERIPHERALS_H_ */
